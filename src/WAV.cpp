﻿#include "WAV.h"
#include <iomanip>

bool isEqual(const unsigned int tag, char* const value)
{
    return tag == *reinterpret_cast<unsigned int*>(value);
}

ostream& operator<<(ostream& os, const header& header)
{
    os << "Header Data:" << endl;
    os << "chunkID" << std::setw(10) << string(header.chunkID, 4) << endl
        << "chunkSize" << std::setw(10) << header.chunkSize << endl
        << "format" << std::setw(10) << string(header.format, 4) << endl
        << "subchunk1ID" << std::setw(10) << string(header.subchunk1ID, 4) << endl
        << "subchunk1Size" << std::setw(10) << header.subchunk1Size << endl
        << "audioFormat" << std::setw(10) << header.audioFormat << endl
        << "numChannels" << std::setw(10) << header.numChannels << endl
        << "sampleRate" << std::setw(10) << header.sampleRate << endl
        << "byteRate" << std::setw(10) << header.byteRate << endl
        << "blockAlign" << std::setw(10) << header.blockAlign << endl
        << "bitsPerSample" << std::setw(10) << header.bitsPerSample << endl;

    return os;
}

bool WAV::read(string path)
{
	bool st{ true };

    fstream fs(path, std::ifstream::in | std::ifstream::binary);
    st &= fs.is_open();
    if (!st) {
        cerr << "Can't open file" << endl; return st;
    }
    st &= readHeader(fs);
    if (!st) {
        cerr << "Can't read header" << endl; return st;
    }
    st &= checkHeader();
    if (!st) {
        cerr << "Bad header" << endl; return st;
    }
    cout << header;
    st &= readSamples(fs);
    if (!st) {
        cerr << "Error while reading samples" << endl; return st;
    }
    return st;
}

bool WAV::readHeader(fstream& fs)
{
	bool st{ true };

    fs.read(reinterpret_cast<char*>(&header), sizeof(header));

    return st;
}

bool WAV::checkHeader()
{
    bool st{ true };

    st &= isEqual(ConstTag::RIFF_TAG, header.chunkID);
    st &= isEqual(ConstTag::WAVE_TAG, header.format);
    st &= isEqual(ConstTag::FMT_TAG, header.subchunk1ID);

    return st;
}

bool WAV::readSamples(fstream& fs)
{
	bool st{ true };

    chunk_t chunk;

    do
    {
        fs.read(reinterpret_cast<char*>(&chunk), sizeof(chunk_t));
    } 
    while (!(isEqual(ConstTag::DATA_TAG, chunk.ID) || fs.eof()));

    if (fs.eof()) {
        cerr << "Can't find data tag" << endl; return false;
    }

    if (header.bitsPerSample != 16){
        cerr << "Only 16 bits per Sample supports" << endl; return false;
    }

    samples.resize(chunk.size * 8 / header.bitsPerSample, 0);

    for (auto& sample : samples)
    {
        // TODO Type of the system? Linux, Windows
        fs.read(reinterpret_cast<char*>(&sample), sizeof(short));
    }

    return st;
}
