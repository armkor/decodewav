#pragma once
#include <list>
#include <string>
#include <vector>
#include <memory>

using namespace std;

class BytesDecoder
{
public:
	BytesDecoder() = default;

	bool decode(shared_ptr<vector<bool>>);
	void print();

private:
	bool findBytesMessage();
	bool decodeBytesMessage();
	unsigned short getByte(int);
	bool getLetter(int, char* letter = nullptr);

	static constexpr int			BYTE_SIZE   = 11;
	static constexpr int			MSG_LEN		= 30;
	static constexpr int			MSG_SIZE	= 64;
	static constexpr unsigned short BYTE_MASK	= 0x600;
	static constexpr unsigned short LETTER_MASK = 0x1FE;
	static constexpr unsigned short STARTBYTE1  = (0x42 << 1) | BYTE_MASK;
	static constexpr unsigned short STARTBYTE2  = (0x03 << 1) | BYTE_MASK;
	static constexpr unsigned short ENDBYTE		= (0x00 << 1) | BYTE_MASK;

	shared_ptr<vector<bool>> bits;
	vector<unsigned short> bytes;
	list<string> messages;
};

