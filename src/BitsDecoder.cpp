﻿#include "BitsDecoder.h"
#include "WAV.h"
#include <iostream>
#include <cstdio>
#include <cmath>
#include <bitset>
#include <map>
#include <string>
#include <list>
#include <string>
#include <algorithm>
#include <iostream>
#include <vector>

bool BitsDecoder::sigToBits(shared_ptr<vector<short>> values)
{
    decode(values);
    return (bits.size() > 0);
}

void BitsDecoderWav1::decode(shared_ptr<vector<short>> values)
{
    if (values->size() == 0) return;
    values->erase(values->begin(), values->begin() + 1);

    vector<int> valuesCh;
    valuesCh.push_back(0);
    auto prev = values->front();

    for (auto val : *values)
    {
        if (val < prev || val > prev)
        {
            valuesCh.push_back(1);
            prev = val;
        }
        else
        {
            valuesCh.back()++;
        }
    }

    bits.reserve(valuesCh.size());

    for (int i = 0; i < valuesCh.size(); i++)
    {
        bits.push_back(valuesCh[i] != 56);
    }
}

void BitsDecoderWav2::decode(shared_ptr<vector<short>> values)
{
    vector<int> valuesCh;
    valuesCh.reserve(values->size());
    valuesCh.push_back(0);
    short prev = *values->begin();

    bool pred = ((*values)[1] < ((*values)[2] + 10) && (*values)[0] < ((*values)[1] + 10));

    for (int i = 1; i < values->size() - 1; i++)
    {
        if (pred == ((*values)[i] < ((*values)[i + 1] + 10) && (*values)[i - 1] < ((*values)[i] + 10)))
        {
            valuesCh.back()++;
        }
        else
        {
            valuesCh.push_back(1);
            pred = !pred;
        }
    }

    bits.reserve(valuesCh.size());

    for (int i = 0; i < valuesCh.size(); i++)
    {
        bits.push_back(valuesCh[i] != 56);
    }
}

void BitsDecoderWav3::decode(shared_ptr<vector<short>> values)
{
    vector<bool> bitSeq;
    bitSeq.reserve(values->size());
    int low{ 0 };
    for (auto val : *values)
    {
        low = lowPass(val);
        if (low != 0)
        {
            bitSeq.push_back(low < 0);
        }
    }

    bool prev = bitSeq.front();

    vector<int> stat;
    stat.reserve(bitSeq.size());
    stat.push_back(0);

    for (auto val : bitSeq)
    {
        if (prev != val)
        {
            prev = val;
            stat.push_back(1);
        }
        else
        {
            stat.back()++;
        }
    }

    bits.reserve(stat.size());
    for (auto val : stat)
    {
        if (val > 25 && val < 32)
        {
            bits.push_back(true);
        }
        else if (val > 51 && val < 60)
        {
            bits.push_back(false);
        }
    }
}

// http://www.sengpielaudio.com/calculator-cutoffFrequencies.htm
// https://www-users.cs.york.ac.uk/~fisher/mkfilter/trad.html
int BitsDecoderWav3::lowPass(short val)
{
    xvLow[0] = xvLow[1]; xvLow[1] = xvLow[2]; xvLow[2] = xvLow[3]; xvLow[3] = xvLow[4]; xvLow[4] = xvLow[5];
    xvLow[5] = val / LOWGAIN;
    yvLow[0] = yvLow[1]; yvLow[1] = yvLow[2]; yvLow[2] = yvLow[3]; yvLow[3] = yvLow[4]; yvLow[4] = yvLow[5];
    yvLow[5] = (xvLow[0] + xvLow[5]) + 5 * (xvLow[1] + xvLow[4]) + 10 * (xvLow[2] + xvLow[3])
        + (0.3591543937 * yvLow[0]) + (-2.1612364569 * yvLow[1])
        + (5.2461675073 * yvLow[2]) + (-6.4284480872 * yvLow[3])
        + (3.9824308930 * yvLow[4]);
    return yvLow[5];
}

int BitsDecoderWav3::highPass(short val)
{
    xvHigh[0] = xvHigh[1]; xvHigh[1] = xvHigh[2]; xvHigh[2] = xvHigh[3]; xvHigh[3] = xvHigh[4]; xvHigh[4] = xvHigh[5];
    xvHigh[5] = val / HIGHGAIN;
    yvHigh[0] = yvHigh[1]; yvHigh[1] = yvHigh[2]; yvHigh[2] = yvHigh[3]; yvHigh[3] = yvHigh[4]; yvHigh[4] = yvHigh[5];
    yvHigh[5] = (xvHigh[5] - xvHigh[0]) + 5 * (xvHigh[1] - xvHigh[4]) + 10 * (xvHigh[3] - xvHigh[2])
        + (-0.0000912600 * yvHigh[0]) + (-0.0557336889 * yvHigh[1])
        + (-0.0034769351 * yvHigh[2]) + (-0.6334736819 * yvHigh[3])
        + (-0.0102622968 * yvHigh[4]);
    return yvHigh[5];
}
