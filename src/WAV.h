#pragma once
#include <string>
#include <iostream> 
#include <fstream>
#include <vector>

using namespace std;

namespace ConstTag
{
    const unsigned int DATA_TAG{ 0x61746164 };
    const unsigned int RIFF_TAG{ 0x46464952 };
    const unsigned int WAVE_TAG{ 0x45564157 };
    const unsigned int FMT_TAG { 0x20746D66 };

    static bool isEqual(const unsigned int, char* const);
};

struct header
{
    char            chunkID[4];
    unsigned long   chunkSize;
    char            format[4];
    char            subchunk1ID[4]; 
    unsigned long   subchunk1Size;
    unsigned short  audioFormat;
    unsigned short  numChannels;
    unsigned long   sampleRate;
    unsigned long   byteRate;
    unsigned short  blockAlign;
    unsigned short  bitsPerSample;

    friend ostream& operator<<(ostream&, const header&);
};

struct chunk_t
{
    char ID[4];
    unsigned long size;
};

class WAV
{
public:
	bool    read(string);
    shared_ptr<vector<short>> get() { return make_shared<vector<short>>(samples); }

private:
    bool readHeader(fstream&);
    bool checkHeader();
	bool readSamples(fstream&);

    header header;
    vector<short> samples;
};

