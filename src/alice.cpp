#include "WAV.h"
#include "BitsDecoder.h"
#include "BytesDecoder.h"
#include <iostream>
#include <cstdio>
#include <cmath>
#include <memory>
#include <map>
#include <string>
#include <list>
#include <string>
#include <algorithm>
#include <iostream>
#include <vector>

bool decode(string path, BitsDecoder* decoder)
{
    if (decoder == nullptr) return false;

    bool st{ true };

    WAV wav;
    st &= wav.read(path);
    if (!st) {
        cerr << "Error while reading wav" << endl; return st;
    }

    st &= decoder->sigToBits(wav.get());
    if (!st) {
        cerr << "Error while bits decoding" << endl; return st;
    }

    BytesDecoder bytesDecoder;
    st &= bytesDecoder.decode(decoder->getBits());
    if (!st) {
        cerr << "Error while bytes decoding" << endl; return st;
    }

    return st;
}

int main()
{
    if (decode("res/file_1.wav", make_shared<BitsDecoderWav1>().get())) {
        cout << "file_1.wav correctly decoded" << endl;
    }
    cout << endl << endl;

    if (decode("res/file_2.wav", make_shared<BitsDecoderWav2>().get())) {
        cout << "file_2.wav correctly decoded" << endl;
    }

    cout << endl << endl;

    if (decode("res/file_3.wav", make_shared<BitsDecoderWav3>().get())) {
        cout << "file_3.wav correctly decoded" << endl;
    }

    return 0;
}