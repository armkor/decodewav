#pragma once
#include <list>
#include <string>
#include <vector>
#include <memory>

using namespace std;

class BitsDecoder
{
public:
	bool sigToBits(shared_ptr<vector<short>>);
	shared_ptr<vector<bool>> getBits() { return make_shared<vector<bool>>(bits); }
	virtual void decode(shared_ptr<vector<short>>) = 0;
protected:
	vector<bool> bits;
};

class BitsDecoderWav1 : public BitsDecoder
{
public:
	void decode(shared_ptr<vector<short>>) override;
};

class BitsDecoderWav2 : public BitsDecoder
{
public:
	void decode(shared_ptr<vector<short>>) override;
};

class BitsDecoderWav3 : public BitsDecoder
{
public:
	void decode(shared_ptr<vector<short>>) override;
private:

	int lowPass(short);
	int highPass(short);

	static constexpr int NZEROS{ 5 };
	static constexpr int NPOLES{ 5 };

	static constexpr float  LOWGAIN{ 1.656528953e+04 };
	static constexpr float  HIGHGAIN{ 1.910018003e+01 };

	float xvLow[NZEROS + 1];
	float yvLow[NPOLES + 1];

	float xvHigh[NZEROS + 1];
	float yvHigh[NPOLES + 1];
};
