﻿#include "BytesDecoder.h"
#include "string.h"
#include "WAV.h"
#include <iostream>
#include <cstdio>
#include <cmath>
#include <bitset>
#include <map>
#include <string>
#include <list>
#include <algorithm>
#include <iostream>
#include <vector>

bool BytesDecoder::decode(shared_ptr<vector<bool>> bitsFlow)
{
    bool st{ true };
    if (!bitsFlow) 
        return false;

    bits = bitsFlow;
    st &= findBytesMessage();
    if (!st) {
        cerr << "Can't find start of messages" << endl; return st;
    }

    st &= (MSG_LEN + 1) * MSG_SIZE == bytes.size();

    if (!st){
        cerr << "Incorrect length of message" << endl; return st;
    }

    st &= decodeBytesMessage();
    if (!st) {
        cerr << "Can't decode bytes Flow" << endl; return st;
    }

    st &= (messages.size() == MSG_SIZE);
    if (!st) {
        cerr << "Incorrect number of messages" << endl; return st;
    }

    print();
    
    return st;
}

void BytesDecoder::print()
{
	for(auto msg: messages)
		cout << msg << std::endl;
}

bool BytesDecoder::findBytesMessage()
{
    bool isFound{false};

    for (int i = 0; i < bits->size() - BYTE_SIZE; )
    {
        unsigned short byte = getByte(i);
        if (isFound)
        {
            if (byte == ENDBYTE) {
                break;
            }
            bytes.push_back(byte);
            i = i + BYTE_SIZE;

        }
        else
        {
            if (byte == STARTBYTE1 && 
                getByte(i + BYTE_SIZE) == STARTBYTE2)
            {
                isFound = true;
                i = i + 2 * BYTE_SIZE;
            }
            else
            {
                i++;
            }
        }
    }

    return isFound;
}

bool BytesDecoder::decodeBytesMessage()
{
    bool st{ true };
    char letter;

    for (int i = 0; i < MSG_SIZE; i++)
    {
        string msg;
        msg.resize(MSG_LEN);

        unsigned short sum = 0;

        for (int j = 0; j < MSG_LEN; j++)
        {
            st &= getLetter((MSG_LEN + 1) * i + j, &letter);
            if (st)
            {
                sum += letter;
                msg[j] = letter;
            }
        }

        
        st &= getLetter((MSG_LEN + 1) * i + MSG_LEN, &letter);
        st &= (letter == (char)(sum & 0xFF));

        if (st)
        {
            messages.push_back(msg);
        }
    }

    return st;
}

unsigned short BytesDecoder::getByte(int index)
{
    std::bitset<BYTE_SIZE> bitSet;
    for (int j = 0; j < BYTE_SIZE; j++)
    {
        bitSet.set(j, (*bits)[index + j]);
    }
    return std::move(bitSet.to_ulong());
}

bool BytesDecoder::getLetter(int index, char* letter)
{
    bool st{ true };

    unsigned short intvalue = bytes[index];

    st = ((BYTE_MASK & intvalue) == BYTE_MASK);

    if (letter != nullptr)
    {
        *letter = ((LETTER_MASK & intvalue) >> 1);
    }

    return st;
}