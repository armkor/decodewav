My steps to resolve challenge

1) Read a lot of information about AFSK and FSK 
2) Install Sonic Visualiser to see data
3) Read about WAV format
4) Make a realization of WAV reading
5) Take a look on data from wav1
6) Understand that this data looks like rectangle
7) Make realization of wav1 decoding
8) Try to decode wav2
9) Find out from data, that it has noise but not longer then 1 element 
10) Make specific realization for it
11) Understand that data for wav3 is very noisy
12) Try to use average filter with 3,5,7,9 size
13) It didn't help
14) Read a lot information about filters for AFSK decoding
15) Rewrite all my code to make it more clear
16) Try to undersrand Butterworth Filter
17) Don't understand how it works correctly
18) Decide to use only lowPass Filter


It helped me a lot just to look on a data. So i used:
1) Sonic Visualiser
2) Visual Studio debugger
3) Visual Studio memory
4) xxd utility from git bash for Windows
5) Also i had a lot of debug files for each iteration, just to understand what is the data